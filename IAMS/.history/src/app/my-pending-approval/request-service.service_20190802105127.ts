import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { PendingRequest } from "./pending-request";
@Injectable({
  providedIn: "root"
})
export class RequestServiceService {

  dummyData : PendingRequest[];

  constructor(private http: HttpClient) {}

  // RequestDetails() {
  //   return this.http.get("http://www.mocky.io/v2/5d375c943100006925b078b6").subscribe((data : any[]) =>{
  //       this.dummyValue = data
  //   })
  // }
  // getRequestDetails(){
  //   console.log(this.dummyValue)
  //   return [...this.dummyValue];
  // }

  // getRequestValue() {
  //   return this.http.get("http://www.mocky.io/v2/5d375c943100006925b078b6");
  // }

  // getRequestById(id :string){
    
  // }
  private _dummyData : BehaviorSubject<RequestPending[]>;
  private baseURL :string;
  private dataStore : {
    dummyData : RequestPending[];
  };
  constructor(private http: HttpClient) {
    this.baseURL ="http://www.mocky.io/v2/5d375c943100006925b078b6";
    this.dataStore = { dummyData : []};
    this._dummyData = <BehaviorSubject<RequestPending[]>>new BehaviorSubject([]);
   }

   get dummyDatas(){
     return this._dummyData.asObservable();
   }

   loadAllRequest(){
     this.http.get(this.baseURL).subscribe((data : RequestPending[])=>{
       this.dataStore.dummyData = data;
       this._dummyData.next(Object.assign({},this.dataStore).dummyData);

     },error=>console.log("could not load data"));
   }

}
