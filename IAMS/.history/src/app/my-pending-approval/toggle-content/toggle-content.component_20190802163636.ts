import { Component, OnInit, Input } from '@angular/core';
import { RequestServiceService } from '../request-service.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'toggle-content',
  templateUrl: './toggle-content.component.html',
  styleUrls: ['./toggle-content.component.scss'],
})
export class ToggleContentComponent implements OnInit {


  dateAndTime: string;
  visible :boolean = false;

  constructor(  private route: ActivatedRoute,
    private requestService: RequestServiceService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      let id = +params["id"];
     this.requestService.dummyData.subscribe(data => {
        data.filter(value => {
         if(value.id===id){
          this.accessType = value.accessType
          this.transactionNo = value.transactionNumber
          this.dateAndTime = value.dateAndTime
         
         }
        });
      });
   
    });
  }


  toggle(){
    this.visible =! this.visible;
  }
}
