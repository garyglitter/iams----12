import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Platform } from '@ionic/angular';
@Component({
  selector: 'app-dash-board',
  templateUrl: './dash-board.page.html',
  styleUrls: ['./dash-board.page.scss'],
})
export class DashBoardPage implements OnInit, OnDestroy, AfterViewInit {

  

  constructor(private statusBar: StatusBar,private platform: Platform) {
    this.statusBar.overlaysWebView(true);
    this.statusBar.backgroundColorByHexString('#1761A0');

     this.platform.registerBackButtonAction(() => {
                    if (this.alertShown == false) {
                        this.presentConfirm();
                        this.alertShown = true
                    }
                }, 0);
   }
   presentConfirm() {
    let alert = this.alertCtrl.create({
        title: 'Confirm',
        message: 'Do you want to exit?',
        buttons: [
            {
                text: 'Cancel',
                role: 'cancel',
                handler: () => {
                    console.log('Cancel clicked');
                    this.alertShown = false;
                }
            },
            {
                text: 'Yes',
                handler: () => {
                    console.log('Yes clicked');
                    this.platform.exitApp();
                }
            }
        ]
    });
    alert.present().then(() => {
        //this.alertShown=true;
    });
}

  ngOnInit() {
 
 
  }
  ngAfterViewInit() { 

  }
  ngOnDestroy() {
    
   }



  tap(){
    console.log("printf")
  }


}
