import { Component,OnDestroy, AfterViewInit,OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { FormsModule } from '@angular/forms';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent implements OnInit, OnDestroy, AfterViewInit {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) { 
    this.initializeApp();
  }
  backButtonSubscription;
  ngOnInit() { }
  ngAfterViewInit(){
   this.backButtonSubscription= this.platform.backButton.subscribe(()=>{
    navigator['app'].exitApp();
    });
  }
  ngOnDestroy() { 
    this.backButtonSubscription.unsubscribe();
  }
  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
